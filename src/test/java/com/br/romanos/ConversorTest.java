package com.br.romanos;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ConversorTest {

    @Test
    public void testaConversor(){
        Conversor conversor = new Conversor();
        conversor.setValor(5);
        conversor.converter();
        Assertions.assertEquals(conversor.getResultado(),"V");
    }
}


