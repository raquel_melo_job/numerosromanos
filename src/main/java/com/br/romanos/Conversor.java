package com.br.romanos;

public class Conversor {

    private String resultado;
    private int valor;

    public Conversor() {
        this.resultado = "";
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public void converter(){
        int quant = 0;
        if(this.valor>= 1000){
            quant = this.valor / 1000;
            for(int i=0;i<quant;i++){
                this.resultado+="M";
            }
            this.setValor(this.valor%(1000*quant));
        }

        if(this.valor>=900){
            this.resultado+="CM";
            this.setValor(this.valor%900);
        }else if(valor>= 500){
            this.resultado+="D";
            this.setValor(this.valor%500);
        }

        if(this.valor>=400){
            this.resultado+="CD";
            this.setValor(this.valor%400);
        }else if(valor>= 100){
            quant = this.valor / 100;
            for(int i=0;i<quant;i++){
                this.resultado+="C";
            }
            this.setValor(this.valor%(100*quant));
        }

        if(this.valor>=90){
            this.resultado+="XC";
            this.setValor(this.valor%90);
        }else if(valor>= 40 && valor<50){
            this.resultado+="XL";
            this.setValor(this.valor%40);
        }else if(valor>=50){
            this.resultado+="L";
            this.setValor(this.valor%50);
        }
        if (this.valor>=10){
            quant = this.valor / 10;
            for(int i=0;i<quant;i++){
                this.resultado+="X";
            }
            this.setValor(this.valor%(10*quant));
        }

        if(this.valor==9){
            this.resultado+="IX";
            this.setValor(this.valor%9);
        }else if(valor== 4){
            this.resultado+="IV";
            this.setValor(this.valor%4);
        }else if(valor>=5){
            this.resultado+="V";
            this.setValor(this.valor%5);
        }
        if (this.valor>=1){
            quant = this.valor;
            for(int i=0;i<quant;i++){
                this.resultado+="I";
            }
        }
    }

}
